export const Greeter = (name: string) => `Hello ${name}`;

export interface IUser {
    _id: string;
    forge_id: string;
    email: string;
    first_name: string;
    last_name: string;
    role: string;
    user_name: string;
    cellphone_number: string;
    phone_number: string;
    secondary_email: string;
    address: IAddress;
    logins: string;
    current_client: string;

}

export interface IAddress {
    street_address: string;
    street_address_2: string;
    suburb: string;
    postcode: string;
    city: string;
    state: string;
    country: string;
}