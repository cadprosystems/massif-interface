export declare const Greeter: (name: string) => string;
export interface IUser {
    name: string;
    firstName: string;
    lastName: string;
}
